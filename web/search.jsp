<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
        <style>
            .description {
                float: right;
            }
        </style>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1 class="align_center"><a href="index.do">MediaNet</a></h1>
                <h3 class="align_center">Rechercher</h3>
                <form name="search" action="search.do" method="GET" style="margin-bottom: 3em;">
                    <div class="grid-row">
                        <input name="document" class="small-12 offset-m-3 medium-6" type="text" placeholder="Titre ou description" value=""/>
                    </div>
                    <div class="grid-row">
                        <select name="type" class="small-12 offset-m-3 medium-3" style="margin-bottom: 1em;">
                            <option value="" selected="selected">Tous</option>
                            <c:forEach var="i" items="${listeType}">
                                <option value="${i.id}">${i.nom}</option>
                            </c:forEach>
                        </select>
                        <select name="genre" class="small-12 medium-3">
                            <option value="" selected="selected">Tous</option>
                            <c:forEach var="i" items="${listeGenre}">
                                <option value="${i.id}">${i.nom}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="grid-row">
                        <input class="button good-width blue raised" type="submit" value="Rechercher"/>
                    </div>
                </form>
<!--                Trier par :
                <input type="radio" name="tri" value="titre" checked="true" >Titre</input>
                <input type="radio" name="tri" value="datep">Date de publication</input>
                <input type="radio" name="tri" value="datea">Date d'ajout</input> --->
                <p><input type="checkbox" name="disponly"/>Disponibles uniquement</p>
                <c:if test="${!empty message}">
                    <p style="margin-bottom: 2em;">${message}</p>
                </c:if>
                
                
                <c:forEach var="i" items="${listeDocument}">
                    <form action="detail.do" method="GET">
                        <div class="l-card small-12 medium-4 align_left" style="margin-bottom: 2em;">
                            <img src="${i.url}" alt="Image${i.id}" title="Image${i.id}"/>
                            <p class="description">${i.etatId.nom}</p>
                            <p><b>${i.titre} de ${i.auteur}</b></p>
                            <p>${i.genreId.nom}</p>
                            <c:if test="${i.etatId.id == 2}">
                                <p class="emprunte">Retour : ${listeDate[i]}</p>
                            </c:if>
                            <p><i class="fa fa-shopping-cart"></i> ${i.datePublication}<span class="description"><i class="fa fa-plus"></i> ${i.dateAjout}</span></p>
                            <input type="hidden" name="id" value="${i.id}"/>
                            <input type="submit" class="small-12 raised blue" style="margin-top: 0.5em; margin-bottom: 0.5em;" value="Voir fiche"/>
                        </div>
                    </form>
                </c:forEach>
            </div>
        </div>
        <%@ include file="javascript.jsp" %>
        <script>
            $("input[name=disponly]").on("change",function(){
               $(".emprunte").each(function(){
                  $(this).closest("form").toggle(); 
               }); 
            });
        </script>
    </body>
</html>
