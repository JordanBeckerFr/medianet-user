<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h3>Liste des documents empruntés</h3>
                <div class="grid-row" style="margin-top: 2em;">
                    <c:choose>
                        <c:when test= "${nombreEmprunt gt 1}">
                            <h5>${nombreEmprunt} documents empruntés</h5>
                        </c:when>
                        <c:when test= "${nombreEmprunt == 1}">
                            <h5>1 documents empruntés au total</h5>
                        </c:when>
                    </c:choose>
                    <table class="small-12 offset-m-2 medium-8">
                        <thead>
                            <tr>
                                <th style="width: 25%">Titre</th>
                                <th style="width: 25%">Auteur</th>
                                <th style="width: 25%">Date d'emprunt</th>
                                <th style="width: 25%">Temps restant</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${empty listeEmprunt}">
                                <tr>
                                    <td colspan="4">Aucun document emprunté actuellement</td>
                                </tr>
                            </c:if>
                            <c:forEach var="i" items="${listeEmprunt}">
                                <tr>
                                    <td>${i.key.documentId.titre}</td>
                                    <td>${i.key.documentId.auteur}</td>
                                    <td>${i.key.dateEmprunt}</td>
                                    <td>${i.value}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>    
                </div>
                <div class="grid-row" style="margin-top: 3em;">
                    <a href="javascript:window.open('','_self').close();" class="button raised blue good-width">Fermer l'onglet</a>  
                </div>
            </div>
        </div>
        <%@ include file="javascript.jsp" %>
    </body>
</html>
