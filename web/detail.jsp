<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h2>${document.titre}</h1>
                <div class="grid-row">
                    <div class="small-12 medium-3">
                        <img src="${document.url}"/>
                    </div>
                    <div class="small-12 medium-9 align_left">
                        <p>Auteur : ${document.auteur}</p>
                        <p>Genre : ${document.genreId.nom}</p>
                        <p>Date d'ajout : ${document.dateAjout}</p>
                        <p>Date de publication : ${document.datePublication}</p>
                        <p>Etat : ${document.etatId.nom}</p>
                        <c:if test="${!empty dateRetour}">
                            <c:choose>
                                <c:when test="${!empty joursRestants}">
                                    <p>Retour dans ${joursRestants} jours, le ${dateRetour}</p>
                                </c:when>
                                <c:when test="${!empty joursEnRetards}">
                                    <p>Retour en retard de ${joursEnRetards} jours, prévu le ${dateRetour}
                                </c:when>    
                            </c:choose> 
                        </c:if>    
                        <p>Description : ${document.description}</p>
                    </div>
                    
                </div>
            </div>
        </div>
        <%@ include file="javascript.jsp" %>
    </body>
</html>
