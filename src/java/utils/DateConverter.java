package utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import model.Emprunt;


public class DateConverter {
    
    /**
     * int, Durée standard des emprunts, en jours
     */
    public static final int DUREE_EMPRUNT=14;
    
    public DateConverter() {
        
    }
    
    /**
     * Calcule la date de rendu maximale d'un document, en prenant en paramètres
     * la date à laquelle ce document à été emprunté
     * @param date Date, Date de début de l'emprunt
     * @return Date, Date de fin de l'emprunt
     */
    public static Date calcDateRendu(Date date) {
        Date newDate=new Date(date.getTime()+(DUREE_EMPRUNT*(1000*60*60*24)));
        return newDate;
    }
    
    /**
     * Calcule le nombre de jours restants avant la date de retour maximale d'un
     * document.
     * Un résultat négatif renseigne le nombre de jours dépassés depuis la date
     * maximale de rendu.
     * @param date Date, Date de début de l'emprunt
     * @return int, Nombre de jours avant ou après la date d'emprunt
     */
    public static int calcJoursRestants(Date date) {
        Date now=new Date();
        Date rendu=DateConverter.calcDateRendu(date);
        int diff=(int)( (rendu.getTime() - now.getTime()) / (1000 * 60 * 60 * 24));
        return diff;
    }
    
    public static String buildUserFriendlySentence(Date date) {
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/YYYY");
        Date endDate=DateConverter.calcDateRendu(date);
        String fullDate=sdf.format(endDate);
        int daysLeft=DateConverter.calcJoursRestants(date);
        
        String res="";
        
        if (daysLeft > 1) {
            res=daysLeft+" jours restants ("+fullDate+")";
        } else if(daysLeft >= 0) {
            res=daysLeft+" jour restant ("+fullDate+")";
        } else if(daysLeft == -1) {
            res=Math.abs(daysLeft)+" jour de retard ("+fullDate+")";
        } else if(daysLeft < -2) {
            res=Math.abs(daysLeft)+" jours de retards ("+fullDate+")";
        }
        
        return res;
    }
    
    public static HashMap<Emprunt, String> generateHashMap(ArrayList<Emprunt> emprunts) {
        HashMap<Emprunt, String> listeEmprunt = new HashMap<>();
        if(emprunts != null) {
            for (Emprunt emprunt : emprunts) {
                listeEmprunt.put(emprunt, buildUserFriendlySentence(emprunt.getDateEmprunt()));
            }
        }
        return listeEmprunt;
    }
}
