package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AdherentRepository {
    
    private ArrayList<Adherent> adherentList;

    public AdherentRepository() {
    }
    
    public ArrayList<Adherent> getAdherentList(Connection conn) {
        
        adherentList = new ArrayList<>();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ADHERENT");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Adherent tmp = new Adherent(rs.getInt("ID"), rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("MAIL"), rs.getDate("DATE_ADHESION"), rs.getString("ADRESSE"));
                adherentList.add(tmp);
            }
        } catch (Exception e) {
            System.out.println("Erreur adherent liste");
        }
        
        return adherentList;
    }
    
    public Adherent getAdherentById(Connection conn, String id) {
        Adherent adherent = null;
        try {
            String query = "SELECT * FROM ADHERENT WHERE ID = ?";
            
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, Integer.parseInt(id));
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            adherent = new Adherent(rs.getInt("ID"), rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("MAIL"), rs.getDate("DATE_ADHESION"), rs.getString("ADRESSE"));
        } catch (NumberFormatException e) {
            System.out.println("Erreur adherent by id");
            return null;
        } catch (SQLException e) {
            System.out.println("Erreur adherent SQLException");
            return null;
        }
        
        return adherent;
    }
    
}
