package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.faces.bean.ApplicationScoped;

@ApplicationScoped
public class DocumentRepository {
    
    private ArrayList<Document> documentList;

    public DocumentRepository() {
    }
    
    public ArrayList<Document> getDocumentList(Connection conn) {
        documentList = new ArrayList<>();
        
        try {
            EtatRepository etatRepository = new EtatRepository();
            GenreRepository genreRepository = new GenreRepository();
            TypeRepository typeRepository = new TypeRepository();
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM DOCUMENT");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Etat etat = etatRepository.getEtatById(conn, Integer.toString(rs.getInt("ETAT_ID")));
                Type type = typeRepository.getTypeById(conn, Integer.toString(rs.getInt("TYPE_ID")));
                Genre genre = genreRepository.getGenreById(conn, Integer.toString(rs.getInt("GENRE_ID")));
                Document tmp = new Document(rs.getInt("ID"), rs.getString("TITRE"), rs.getString("AUTEUR"), rs.getString("DESCRIPTION"), rs.getString("URL"), rs.getDate("DATE_AJOUT"), rs.getDate("DATE_PUBLICATION"), etat, genre, type);
                documentList.add(tmp);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return documentList;
    }
    
    public Document getDocumentById(Connection conn, String id) {
        Document document = null;
        try {
            EtatRepository etatRepository = new EtatRepository();
            GenreRepository genreRepository = new GenreRepository();
            TypeRepository typeRepository = new TypeRepository();
            String query = "SELECT * FROM DOCUMENT WHERE ID = ?";
            
            PreparedStatement stmt = conn.prepareStatement(query);
            try {
                stmt.setInt(1, Integer.parseInt(id));
            } catch (Exception e) {
                return null;
            }
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            Etat etat = etatRepository.getEtatById(conn, Integer.toString(rs.getInt("ETAT_ID")));
            Type type = typeRepository.getTypeById(conn, Integer.toString(rs.getInt("TYPE_ID")));
            Genre genre = genreRepository.getGenreById(conn, Integer.toString(rs.getInt("GENRE_ID")));
            document = new Document(rs.getInt("ID"), rs.getString("TITRE"), rs.getString("AUTEUR"), rs.getString("DESCRIPTION"), rs.getString("URL"), rs.getDate("DATE_AJOUT"), rs.getDate("DATE_PUBLICATION"), etat, genre, type);
        
        } catch (NumberFormatException e) {
            System.out.println("Erreur document by id");
            return null;
        } catch (SQLException e) {
            System.out.println("Erreur document SQLException");
            return null;
        }
        
        return document;
    }
    
    public ArrayList<Document> getDocumentListByParam(Connection conn, HashMap<String, String> parameters) {
        documentList = new ArrayList<>();
        
        HashMap<String, String> paramListe = new HashMap<>();
        
        String[] nomFiltre = new String[] {"document", "genre", "type", "disponible", "user"};
        for(String str : nomFiltre) {
            if(parameters.containsKey(str)) {
                if(parameters.get(str).length() > 0) {
                    paramListe.put(str, parameters.get(str));
                }
            }
        }
        
        try {
            
            String query = "SELECT * FROM DOCUMENT";
            int count = 0;
            for(String key : paramListe.keySet()) {
                
                if(count == 0) {
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }
                
                switch (key) {
                    case "document":
                        query += "(titre LIKE ? OR description LIKE ?)";
                        break;
                    case "genre":
                        query += "genre_id = ?";
                        break;
                    case "type":
                        query += "type_id = ?";
                        break;
                    case "user":
                        query += "user_id = ?";
                        break;
                    default:
                        break;
                }
                count++;
            }
                
            EtatRepository etatRepository = new EtatRepository();
            GenreRepository genreRepository = new GenreRepository();
            TypeRepository typeRepository = new TypeRepository();
            PreparedStatement stmt = conn.prepareStatement(query);
            
            count = 1;
            for(String key : paramListe.keySet()) {
                switch (key) {
                    case "document":
                        stmt.setString(count, "%"+paramListe.get(key)+"%");
                        count++;
                        stmt.setString(count, "%"+paramListe.get(key)+"%");
                        break;
                    case "user":
                        stmt.setString(count, paramListe.get(key));
                        break;
                    case "genre":
                    case "type":
                        stmt.setInt(count, Integer.parseInt(paramListe.get(key)));
                    default:
                        break;
                }
                count++;
            }
            
            stmt.executeQuery();
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Etat etat = etatRepository.getEtatById(conn, Integer.toString(rs.getInt("ETAT_ID")));
                Type type = typeRepository.getTypeById(conn, Integer.toString(rs.getInt("TYPE_ID")));
                Genre genre = genreRepository.getGenreById(conn, Integer.toString(rs.getInt("GENRE_ID")));
                Document document = new Document(rs.getInt("ID"), rs.getString("TITRE"), rs.getString("AUTEUR"), rs.getString("DESCRIPTION"), rs.getString("URL"), rs.getDate("DATE_AJOUT"), rs.getDate("DATE_PUBLICATION"), etat, genre, type);
                documentList.add(document);
            }
            
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return documentList;
    }
    
}
