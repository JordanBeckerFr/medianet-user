package model;

import java.util.Collection;

public class Type {
    
    private Integer id;
    
    private String nom;
    
    private Collection<Document> documentCollection;

    public Type() {
    }

    public Type(Integer id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Type(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Collection<Document> getDocumentCollection() {
        return documentCollection;
    }

    public void setDocumentCollection(Collection<Document> documentCollection) {
        this.documentCollection = documentCollection;
    }
    
}
