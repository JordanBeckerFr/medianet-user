package model;

import java.io.Serializable;
import java.util.Date;

public class Emprunt {
    
    private Integer id;
    
    private Date dateEmprunt;
    
    private Date dateRetour;
    
    private Adherent adherentId;
    
    private Document documentId;

    public Emprunt() {
    }

    public Emprunt(Integer id, Date dateEmprunt, Date dateRetour, Adherent adherent, Document document) {
        this.id = id;
        this.dateEmprunt = dateEmprunt;
        this.dateRetour = dateRetour;
        this.adherentId = adherent;
        this.documentId = document;
    }

    public Emprunt(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    public Date getDateRetour() {
        return dateRetour;
    }

    public void setDateRetour(Date dateRetour) {
        this.dateRetour = dateRetour;
    }

    public Adherent getAdherentId() {
        return adherentId;
    }

    public void setAdherentId(Adherent adherentId) {
        this.adherentId = adherentId;
    }

    public Document getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Document documentId) {
        this.documentId = documentId;
    }

}
