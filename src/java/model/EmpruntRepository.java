package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.enterprise.context.ApplicationScoped;
import exception.UnknownAdherentException;

@ApplicationScoped
public class EmpruntRepository {
    
    private ArrayList<Emprunt> empruntList;

    public EmpruntRepository() {
    }
    
    public ArrayList<Emprunt> getEmpruntList(Connection conn) {
        empruntList = new ArrayList<>();
        
        try {
            DocumentRepository documentRepository = new DocumentRepository();
            AdherentRepository adherentRepository = new AdherentRepository();
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM EMPRUNT");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Document document = documentRepository.getDocumentById(conn, Integer.toString(rs.getInt("DOCUMENT_ID")));
                Adherent adherent = adherentRepository.getAdherentById(conn, Integer.toString(rs.getInt("ADHERENT_ID")));
                Emprunt tmp = new Emprunt(rs.getInt("ID"), rs.getDate("DATE_EMPRUNT"), rs.getDate("DATE_RETOUR"), adherent, document);
                empruntList.add(tmp);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return empruntList;
    }
    
    public ArrayList<Emprunt> getEmpruntListByAdherent(Connection conn,String id_Adherent) {
        empruntList = new ArrayList<>();
        try {
            DocumentRepository documentRepository = new DocumentRepository();
            AdherentRepository adherentRepository = new AdherentRepository();
            
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM EMPRUNT WHERE ADHERENT_ID = ?");
            stmt.setInt(1, Integer.parseInt(id_Adherent));
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Document document = documentRepository.getDocumentById(conn, Integer.toString(rs.getInt("DOCUMENT_ID")));
                Adherent adherent = adherentRepository.getAdherentById(conn, Integer.toString(rs.getInt("ADHERENT_ID")));
                Emprunt emprunt = new Emprunt(rs.getInt("ID"), rs.getDate("DATE_EMPRUNT"), rs.getDate("DATE_RETOUR"), adherent, document);
                
                empruntList.add(emprunt);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }
        
        return empruntList;
        
    }
    
    public ArrayList<Emprunt> getEmpruntListTriee(Connection conn,ArrayList<Emprunt> list,int id_Etat){
        empruntList = new ArrayList<>();
        DocumentRepository documentRepository = new DocumentRepository();
        for(Emprunt emprunt : list){
            Document document = emprunt.getDocumentId();
            if(document.getEtatId().getId() == id_Etat) {
                empruntList.add(emprunt);
            }
        }
        return empruntList;
    }
    
    public ArrayList<Emprunt> getEmpruntListByParam(Connection conn, HashMap<String, String> parameters) {
        empruntList = new ArrayList<>();
        
        HashMap<String, String> paramListe = new HashMap<>();
        
        String[] nomFiltre = new String[] {"id", "adherent", "document", "retourne", "non-retourne"};
        for(String str : nomFiltre) {
            if(parameters.containsKey(str)) {
                if(parameters.get(str).length() > 0) {
                    paramListe.put(str, parameters.get(str));
                }
            }
        }
        
        try {
            String query = "SELECT * FROM EMPRUNT";
            int count = 0;
            for(String key : paramListe.keySet()) {
                
                if(count == 0) {
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }
                
                switch (key) {
                    case "id":
                        query += "id = ?";
                        break;
                    case "adherent":
                        query += "adherent_id = ?";
                        break;
                    case "document":
                        query += "document_id = ?";
                        break;
                    case "retourne":
                        query += "date_retour IS NOT NULL";
                        break;
                    case "non-retourne":
                        query += "date_retour IS NULL";
                        break;
                    default:
                        break;
                }
                count++;
            }
                
            PreparedStatement stmt = conn.prepareStatement(query);
            
            count = 1;
            try {
                for(String key : paramListe.keySet()) {
                    switch (key) {
                        case "id":
                            stmt.setInt(count, Integer.parseInt(paramListe.get(key)));
                            break;
                        case "adherent":
                            stmt.setInt(count, Integer.parseInt(paramListe.get(key)));
                            break;
                        case "document":
                            stmt.setInt(count, Integer.parseInt(paramListe.get(key)));
                        case "retourne":
                        case "non-retourne":
                            count--;
                            break;
                        default:
                            break;
                    }
                    count++;
                }
            } catch (NumberFormatException e) {
                return null;
            }
            
            stmt.executeQuery();
            ResultSet rs = stmt.executeQuery();
            
            AdherentRepository adherentRepository=new AdherentRepository();
            DocumentRepository documentRepository=new DocumentRepository();
            
            while(rs.next()){
                Adherent adherent = adherentRepository.getAdherentById(conn, Integer.toString(rs.getInt("ADHERENT_ID")));
                Document document = documentRepository.getDocumentById(conn, Integer.toString(rs.getInt("DOCUMENT_ID")));
                Emprunt emprunt = new Emprunt(rs.getInt("ID"), rs.getDate("DATE_EMPRUNT"), rs.getDate("DATE_RETOUR"), adherent, document);
                empruntList.add(emprunt);
            }
            
            
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return empruntList;
    }
    
}
