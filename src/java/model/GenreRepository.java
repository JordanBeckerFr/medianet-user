package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GenreRepository {
    
    private ArrayList<Genre> genreList;

    public GenreRepository() {
    }
    
    public ArrayList<Genre> getGenreList(Connection conn) {
        genreList = new ArrayList<>();
        
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM GENRE");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Genre tmp = new Genre(rs.getInt("ID"), rs.getString("NOM"));
                genreList.add(tmp);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return genreList;
    }
    
    public Genre getGenreById(Connection conn,String id){
        Genre genre= null;
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM GENRE WHERE ID = ?");
            stmt.setInt(1,Integer.parseInt(id));
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            genre = new Genre(rs.getInt("ID"),rs.getString("NOM"));
        } catch (NumberFormatException e) {
            System.out.println("Erreur genre by id");
            return null;
        } catch (SQLException e) {
            System.out.println("Erreur genre SQLException");
            return null;
        }
        return genre;
    }
}
