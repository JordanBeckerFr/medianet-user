package model;


import java.util.Collection;
import java.util.Date;

public class Document {
    
    private Integer id;
    
    private String titre;
    
    private String auteur;
    
    private String description;
    
    private String url;
    
    private Date dateAjout;
    
    private Date datePublication;
    
    private Collection<Emprunt> empruntCollection;
    
    private Etat etatId;
    
    private Genre genreId;
    
    private Type typeId;

    public Document() {
    }

    public Document(Integer id, String titre, String auteur, String description, String url, Date dateAjout, Date datePublication, Etat etat, Genre genre, Type type) {
        this.id = id;
        this.titre = titre;
        this.auteur = auteur;
        this.description = description;
        this.url = url;
        this.dateAjout = dateAjout;
        this.datePublication = datePublication;
        this.etatId = etat;
        this.genreId = genre;
        this.typeId = type;
    }

    public Document(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public Collection<Emprunt> getEmpruntCollection() {
        return empruntCollection;
    }

    public void setEmpruntCollection(Collection<Emprunt> empruntCollection) {
        this.empruntCollection = empruntCollection;
    }

    public Etat getEtatId() {
        return etatId;
    }

    public void setEtatId(Etat etatId) {
        this.etatId = etatId;
    }

    public Genre getGenreId() {
        return genreId;
    }

    public void setGenreId(Genre genreId) {
        this.genreId = genreId;
    }

    public Type getTypeId() {
        return typeId;
    }

    public void setTypeId(Type typeId) {
        this.typeId = typeId;
    }

}
