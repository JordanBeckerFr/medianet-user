package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TypeRepository {
    
    private ArrayList<Type> typeList;

    public TypeRepository() {
    }
    
    public ArrayList<Type> getTypeList(Connection conn) {
        typeList = new ArrayList<>();
        
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM TYPE");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Type tmp = new Type(rs.getInt("ID"), rs.getString("NOM"));
                typeList.add(tmp);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return typeList;
    }
    
    public Type getTypeById(Connection conn,String id){
        Type type = null;
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM TYPE WHERE ID = ? ");
            stmt.setInt(1, Integer.parseInt(id));
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            type = new Type(rs.getInt("ID"),rs.getString("NOM"));
        } catch (NumberFormatException e) {
            System.out.println("Erreur type by id");
            return null;
        } catch (SQLException e) {
            System.out.println("Erreur type SQLException");
            return null;
        }
        return type;
    }
    
}
