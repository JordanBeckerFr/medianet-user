package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.Document;
import model.DocumentRepository;
import model.GenreRepository;
import model.TypeRepository;
import exception.UnknownDocumentException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Emprunt;
import model.EmpruntRepository;
import utils.DateConverter;

public class DetailCommand implements Command {

    private TypeRepository typeRepository;
    private GenreRepository genreRepository;
    private DocumentRepository documentRepository;
    private EmpruntRepository empruntRepository;
    
    @Override
    public String getCommandName() {
        return "detail";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "detail";
        
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        
        typeRepository = new TypeRepository();
        genreRepository = new GenreRepository();
        documentRepository = new DocumentRepository();
        empruntRepository = new EmpruntRepository();
        
        
        req.setAttribute("listeType", typeRepository.getTypeList((Connection)servletContext.getAttribute("conn")));
        req.setAttribute("listeGenre", genreRepository.getGenreList((Connection)servletContext.getAttribute("conn")));
        
        try {
            Document document = documentRepository.getDocumentById((Connection)servletContext.getAttribute("conn"), parameters.get("id"));
            if(document == null) {
                throw new UnknownDocumentException();
            } else {
                req.setAttribute("document", document);
                if (document.getEtatId().getId()==2){
                    HashMap<String,String> parametersEmprunt = new HashMap<String,String>();
                    parametersEmprunt.put("non-retourne", "1");
                    parametersEmprunt.put("document", document.getId().toString());
                    ArrayList<Emprunt> emprunt = empruntRepository.getEmpruntListByParam((Connection)servletContext.getAttribute("conn"), parametersEmprunt);
                    Date dateEmprunt = emprunt.get(0).getDateEmprunt();
                    int joursRestants = DateConverter.calcJoursRestants(dateEmprunt);
                    SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-YYYY");
                    String dateRetour = dateFormater.format(DateConverter.calcDateRendu(dateEmprunt));
                    req.setAttribute("dateRetour", dateRetour);
                    if(joursRestants >= 0){
                        req.setAttribute("joursRestants",joursRestants);
                    }else{
                        req.setAttribute("joursEnRetards",Math.abs(joursRestants));
                    }
                       
                    
                }
            }
        } catch (UnknownDocumentException e) {
            req.setAttribute("message", "Erreur : Le document demandé n'a pas été trouvé");
            return new ActionFlow("Erreur", "search.do", false);
        }
        
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
