package command;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.Emprunt;
import model.EmpruntRepository;
import utils.DateConverter;

public class SummaryCommand implements Command {

    private EmpruntRepository empruntRepository;
    
    @Override
    public String getCommandName() {
        return "summary";
    }

    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "summary";
        
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        
        empruntRepository = new EmpruntRepository();        
        
        parameters.put("non-retourne", "1");
        ArrayList<Emprunt> emprunts = empruntRepository.getEmpruntListByParam((Connection)servletContext.getAttribute("conn"), parameters);
        
        HashMap<Emprunt, String> listeEmprunt = DateConverter.generateHashMap(emprunts);
        req.setAttribute("listeEmprunt", listeEmprunt);
        
        if(emprunts != null) {
            req.setAttribute("nombreEmprunt", listeEmprunt.size());
        } else {
            req.setAttribute("nombreEmprunt", "0");
        }
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
