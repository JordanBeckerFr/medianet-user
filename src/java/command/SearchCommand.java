package command;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.Document;
import model.DocumentRepository;
import model.Emprunt;
import model.EmpruntRepository;
import model.GenreRepository;
import model.TypeRepository;
import utils.DateConverter;

public class SearchCommand implements Command {
    
    private TypeRepository typeRepository;
    private GenreRepository genreRepository;
    private DocumentRepository documentRepository;
    private EmpruntRepository empruntRepository;
    
    @Override
    public String getCommandName() {
        return "search";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "search";
        
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        
        typeRepository = new TypeRepository();
        genreRepository = new GenreRepository();
        documentRepository = new DocumentRepository();
        empruntRepository = new EmpruntRepository();
        
        req.setAttribute("listeType", typeRepository.getTypeList((Connection)servletContext.getAttribute("conn")));
        req.setAttribute("listeGenre", genreRepository.getGenreList((Connection)servletContext.getAttribute("conn")));
        ArrayList<Document> listeDocument = documentRepository.getDocumentListByParam((Connection)servletContext.getAttribute("conn"), parameters);
        
        
        
        if(listeDocument.size() == 0) {
            req.setAttribute("message", "Erreur : Aucun document trouvé");
        }
        HashMap<Document,String> listeDate = new HashMap<Document,String>();
        for(Document document: listeDocument){
           if (document.getEtatId().getId()==2){
                    HashMap<String,String> parametersEmprunt = new HashMap<String,String>();
                    parametersEmprunt.put("non-retourne", "1");
                    parametersEmprunt.put("document", document.getId().toString());
                    ArrayList<Emprunt> emprunt = empruntRepository.getEmpruntListByParam((Connection)servletContext.getAttribute("conn"), parametersEmprunt);
                    Date dateEmprunt = emprunt.get(0).getDateEmprunt();
                    SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-YYYY");
                    String dateRetour = dateFormater.format(DateConverter.calcDateRendu(dateEmprunt));
                    listeDate.put(document,dateRetour);
                } 
        }
        req.setAttribute("listeDocument", listeDocument);
        req.setAttribute("listeDate", listeDate);
        return new ActionFlow(vue, vue+".jsp", false);
    }
   
}
