package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.GenreRepository;
import model.TypeRepository;

public class IndexCommand implements Command {

    private GenreRepository genreRepository;
    private TypeRepository typeRepository;
    
    @Override
    public String getCommandName() {
        return "index";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "index";
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        HttpServletResponse res = (HttpServletResponse) configurations.get("response");
        
        genreRepository = new GenreRepository();
        typeRepository = new TypeRepository();
        
        req.setAttribute("listeType", typeRepository.getTypeList((Connection)servletContext.getAttribute("conn")));
        req.setAttribute("listeGenre", genreRepository.getGenreList((Connection)servletContext.getAttribute("conn")));
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
