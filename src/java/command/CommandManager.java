package command;

import java.util.HashMap;

public class CommandManager {

    
    public CommandManager() {
        init();
    }
    
    HashMap<String, Command> commandMap;

    public void init() {
        this.commandMap = new HashMap<>();
        this.commandMap.put("index", new IndexCommand());
        this.commandMap.put("search", new SearchCommand());
        this.commandMap.put("detail", new DetailCommand());
        this.commandMap.put("account", new AccountCommand());
        this.commandMap.put("summary", new SummaryCommand());
    }
    
    public Command getCommand(String cmd) {
        System.out.println(commandMap.get(cmd).getClass());
        return this.commandMap.get(cmd);
    }
}
