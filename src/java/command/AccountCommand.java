package command;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.EmpruntRepository;
import model.Emprunt;
import exception.UnknownAdherentException;
import model.Adherent;
import model.AdherentRepository;
import utils.DateConverter;

public class AccountCommand implements Command {

    private EmpruntRepository empruntRepository;
    private AdherentRepository adherentRepository;
    
    @Override
    public String getCommandName() {
        return "account";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "account";
        
        empruntRepository = new EmpruntRepository();
        adherentRepository = new AdherentRepository();
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        ArrayList<Emprunt> emprunts = null;
        
        try {
            if(parameters.get("adherent").trim().length() == 0) {   
                throw new UnknownAdherentException();
            }
            parameters.put("non-retourne", "1");
            Adherent adherent = adherentRepository.getAdherentById((Connection)servletContext.getAttribute("conn"), parameters.get("adherent"));
            if(adherent == null) {
                throw new UnknownAdherentException();
            } else {
                emprunts = empruntRepository.getEmpruntListByParam((Connection)servletContext.getAttribute("conn"), parameters);
                
            }
        } catch (UnknownAdherentException e) {
            req.setAttribute("messageConnexion", "Erreur : Le compte adhérent n'a pas été retrouvé");
            return new ActionFlow("Erreur", "index.do", false);
        }
        
        HashMap<Emprunt, String> listeEmprunt = DateConverter.generateHashMap(emprunts);
        req.setAttribute("listeEmprunt", listeEmprunt);
        
        //Suppression du paramètre des documents non retournés
        parameters.remove("non-retourne");
        
        //Ajout du paramètre pour sortir les documents retournés
        parameters.put("retourne", "1");
        
        HashMap<Emprunt, String> listeEmpruntRendu = DateConverter.generateHashMap(empruntRepository.getEmpruntListByParam((Connection)servletContext.getAttribute("conn"), parameters));
        req.setAttribute("listeEmpruntRendu", listeEmpruntRendu);
        
        Adherent user = adherentRepository.getAdherentById((Connection)servletContext.getAttribute("conn"), parameters.get("adherent"));
        req.setAttribute("adherentObject", user);
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
