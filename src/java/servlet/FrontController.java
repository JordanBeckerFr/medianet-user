package servlet;

import command.*;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ControleurServlet", urlPatterns = {"*.do"})
public class FrontController extends HttpServlet{
    
    private Command command;
    private CommandManager commandManager;
    
    public void init() {
        this.command = null;
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { 
        doPost(req, res);
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        this.commandManager = new CommandManager();
        ActionFlow actionFlow = new ActionFlow("Erreur", req.getContextPath()+"/error.html", true);
        
        ServletContext context = getServletContext();
        HashMap<String, String> parameters = extractParameters(req);
        HashMap<String, Object> conf = new HashMap<>();
        conf.put("servletContext", context);
        conf.put("request", req);
        conf.put("response", res);
        String commandString = getOperation(req.getServletPath());
        if(commandString != null) {
            this.command = this.commandManager.getCommand(commandString);
            if(this.command != null) {
                actionFlow = this.command.execute(parameters, conf);
            }
        }
        
        if(actionFlow.isRedirect() == true) {
            res.sendRedirect(actionFlow.getPath());
        } else {
            RequestDispatcher rd = context.getRequestDispatcher("/"+actionFlow.getPath());
            rd.forward(req, res);
        }
    }
    
    private String getOperation(String uri) {
        System.out.println(uri);
        Pattern p = Pattern.compile(".*/(.*).do");
        Matcher m = p.matcher(uri);
        if(m.matches() && m.groupCount() == 1) {
            return m.group(1);
        } else {
            return null;
        }
    }
    
    private HashMap<String, String> extractParameters(HttpServletRequest req) {
        HashMap<String, String> parameters = new HashMap<>();
        Enumeration<String> names = req.getParameterNames();
        
        while(names.hasMoreElements()) {
            String name = names.nextElement();
            String value = req.getParameter(name);
            if (value != null) {
                parameters.put(name, value);
            }
        }
        
        return parameters;
        
    }
}
