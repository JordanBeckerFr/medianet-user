package exception;

import java.util.ArrayList;
import model.Emprunt;

public class UnknownDocumentException extends Exception{
    
    private ArrayList<Integer> document_inconnus;
    private ArrayList<Emprunt> liste_emprunts;

    public UnknownDocumentException() {
        System.out.println("UnknownDocumentException");
    }
    
    public UnknownDocumentException(ArrayList<Integer> document_inconnus,ArrayList<Emprunt> liste_emprunts){
        super("Certains documents sont inconnus");
        this.document_inconnus=document_inconnus;
        this.liste_emprunts=liste_emprunts;
    }
    
    public ArrayList<Integer> getDocument_inconnus() {
        return document_inconnus;
    }

    public ArrayList<Emprunt> getListe_emprunts() {
        return liste_emprunts;
    }
    
}
