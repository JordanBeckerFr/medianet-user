package exception;

public class UnknownAdherentException extends Exception{
    public UnknownAdherentException(){
        super("L'adhérent spécifié n'existe pas ");
        System.out.println("UnknownAdherentException");
    }
}
